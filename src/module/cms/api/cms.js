import http from '../../../base/api/public'

export const page_list = (page, size, params) => {
    return new Promise((resolve, reject) => {
        http.requestGet("api/cms/page/list/" + page + "/" + size, params).then((res) => {
            resolve(res);
        }).catch((error) => {
            reject(error);
        })
    })
};
export const page_add = (params) => {
    return new Promise((resolve, reject) => {
        http.requestPost("api/cms/page/add/", params).then((res) => {
            resolve(res);
        }).catch((error) => {
            reject(error);
        })
    })
};
//GET /cms/page/get/{id
export const page_get_id = (params) => {
    return new Promise((resolve, reject) => {
        http.requestGet("api/cms/page/get/"+ params).then((res) => {
            resolve(res);
        }).catch((error) => {
            reject(error);
        })
    })
};
//PUT /cms/page/edit/{id}
export const page_edit = (params,data) => {
    console.log(params,data);
    return new Promise((resolve, reject) => {
        http.requestPut("api/cms/page/edit/"+ params,data).then((res) => {
            resolve(res);
        }).catch((error) => {
            reject(error);
        })
    })
};
//PUT /cms/page/edit/{id}
export const page_deleteById = (id) => {
    console.log();id
    return new Promise((resolve, reject) => {
        http.requestDelete("api/cms/page/delete/"+id).then((res) => {
            resolve(res);
        }).catch((error) => {
            reject(error);
        })
    })
};