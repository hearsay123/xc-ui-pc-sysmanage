import {page_list, page_add, page_get_id, page_edit} from '../api/cms'
import {Message} from "element-ui"

const OK = 10000;
const state = {
    list: [{
        siteId: "",
        pageId: "",
        pageName: "",
        pageAliase: "",
        pageWebPath: "",
        pageParameter: null,
        pagePhysicalPath: "",
        pageType: "",
        pageTemplate: null,
        pageHtml: null,
        pageStatus: null,
        pageCreateTime: "",
        templateId: "",
        pageParams: [
            {
                pageParamName: "",
                pageParamValue: ""
            }
        ],
        htmlFileId: "",
        dataUrl: ""
    },],
    total: null,
    page_get_id: {},
    pageEditUrl:{
        page: '',siteId: ''
    }
};

const mutations = {
    addList(state, queryResult) {
        state.list = queryResult.list;
        state.total = queryResult.total;
    },
    addPage_get_id(state, res) {
        state.page_get_id = res;
    },
    setPageEditUrl(state,data){
        state.pageEditUrl = data;
    }
};
const actions = {
    async getPage_list({commit}, {params, parames}) {
        try {
            let res = await page_list(params.page, params.size, parames);
            if (OK === res.code) {
                commit('addList', res.queryResult)
            }
            // obj.page===1&&Message('查询成功');
        } catch (e) {
            Message('服务器错误');
        }
    },
    addPage_add({}, pageForm) {
        return page_add(pageForm)
            .then(data => {
                if (data.success) {
                    Message('添加成功');
                    return 1
                }
                Message('添加失败');
                return 0;
            }).catch(err => {
                Message('添加失败');
                return 0;
            })
    },
    async pageGetId({commit}, id) {
        let res = await page_get_id(id).catch((err) => err);
        if (!res.response) {
            commit('addPage_get_id', res)
            return res;
        }
        Message('数据获取失败');
        return null;
    },
   async pageEdit({},{id,form}){
       let res =  await page_edit(id,form).catch(e=>e);
       console.log(res);
       if(!res.response){
           Message('数据修改成功');
         return res;
       }
       Message('数据修改失败');
       return null;
    }
};
export default {
    actions,
    state,
    mutations
};
