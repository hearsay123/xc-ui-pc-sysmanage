import Home from '../../home/page/home';
import page_list from '../page/page_list.vue';

export default [{
    path: '/',
    component: Home,
    name: 'cms',
    hidden: false,
    children: [
        {path: '/cms/page/list', name: '页面列表', component: page_list, hidden: false, props: (route) => ({ query: route.query })},
        {path: '/cms/page/add', name: '新增页面', component:() => import('../page/page_add.vue'), hidden: true},
        {path: '/cms/page/edit/:pageId', name: '修改页面', component:() => import('../page/page_edit.vue'), hidden: true},
    ]
}/*,
  {
    path: '/login',
    component: Login,
    name: 'Login',
    hidden: true
  }*/
]
