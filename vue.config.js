const path = require('path');
const resolve = dir => path.join(__dirname, dir);
const BASE_URL = process.env.NODE_ENV === 'production' ? '/' : '/';

module.exports = {
    lintOnSave: false,
    publicPath: BASE_URL,
    // baseUrl: BASE_URL,
    // 打包不需要map
    // productionSourceMap
    productionSourceMap: false,
    /*配置跨域*/
    devServer: {
        proxy: {
            "api/cms": {
                target: 'http://localhost:31001',
                pathRewrite: {
                    'api': ''
                },
            },
        }
    }
};
